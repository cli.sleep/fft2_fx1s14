FROM ubuntu:18.04
RUN apt update && apt install build-essential -y

COPY . /tmp/build
WORKDIR /tmp/build

CMD make clean default

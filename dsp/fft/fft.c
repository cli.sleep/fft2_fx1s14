#include "fft.h"
#include "../dsp.h"
#include <assert.h>
#include <emmintrin.h>
#include <tmmintrin.h>

#include "twd_lut.h"

static const size_t max_len = DSP_MAX_FFT_LEN;

static inline int is_pow2(size_t i) { return (i != 0) && !(i & (i - 1)); }

// Multiply two complex vectors of size 4
static inline __m128i vcmul4(__m128i a, __m128i b) {
  __m128i re, im;
  re = _mm_shufflelo_epi16(a, _MM_SHUFFLE(2, 2, 0, 0));
  re = _mm_shufflehi_epi16(re, _MM_SHUFFLE(2, 2, 0, 0));
  re = _mm_mulhrs_epi16(re, b);
  im = _mm_shufflelo_epi16(a, _MM_SHUFFLE(3, 3, 1, 1));
  im = _mm_shufflehi_epi16(im, _MM_SHUFFLE(3, 3, 1, 1));
  b = _mm_shufflelo_epi16(b, _MM_SHUFFLE(2, 3, 0, 1));
  b = _mm_shufflehi_epi16(b, _MM_SHUFFLE(2, 3, 0, 1));
  im = _mm_mulhrs_epi16(im, b);
  im = _mm_sign_epi16(im, _mm_set_epi16(1, -1, 1, -1, 1, -1, 1, -1));

  return _mm_adds_epi16(re, im);
}

void fft_fx1s14(fft_cplx_t *in, size_t n) {
  assert(n >= 8 && n <= max_len);
  assert(is_pow2(n));

  int16_t const(**twd)[2] = twd_lut;
  for (size_t k = 0; k < n; k += 8) {
    __m128i v0 = _mm_loadu_si128((__m128i *)&in[k]);
    __m128i v1 = _mm_loadu_si128((__m128i *)&in[k + 4]);
    {
      // stage 1: no multiplication

      // arithmetic shift right to avoid saturation
      v0 = _mm_srai_epi16(v0, 1);
      v1 = _mm_srai_epi16(v1, 1);

      // rearrange real and imaginary parts
      v0 = _mm_shufflelo_epi16(v0, _MM_SHUFFLE(3, 1, 2, 0));
      v0 = _mm_shufflehi_epi16(v0, _MM_SHUFFLE(3, 1, 2, 0));
      v1 = _mm_shufflelo_epi16(v1, _MM_SHUFFLE(3, 1, 2, 0));
      v1 = _mm_shufflehi_epi16(v1, _MM_SHUFFLE(3, 1, 2, 0));

      // horizontal addition and subtraction
      __m128i e = _mm_hadds_epi16(v0, v1);
      __m128i o = _mm_hsubs_epi16(v0, v1);

      // output: e+o, e-o ...
      v0 = _mm_unpacklo_epi32(e, o);
      v1 = _mm_unpackhi_epi32(e, o);
    }
    {
      // stage 2
      __m128i w = _mm_loadu_si128((__m128i *)*twd);
      // duplicate twiddle factors for this stage
      w = _mm_shuffle_epi32(w, _MM_SHUFFLE(1, 0, 1, 0));

      // rearrange result of previous stage to get continuous 4 element complex
      // vectors
      __m128i e = _mm_unpacklo_epi64(v0, v1);
      __m128i o = _mm_unpackhi_epi64(v0, v1);

      // complex multiply 4 odd elements with 4 twiddle factors
      // this also scales input down by 2
      __m128i t = vcmul4(o, w);

      // scale down even elements
      e = _mm_srai_epi16(e, 1);

      // add, subtract
      o = _mm_subs_epi16(e, t);
      e = _mm_adds_epi16(e, t);

      _mm_storeu_si128((__m128i *)&in[k], _mm_unpacklo_epi64(e, o));
      _mm_storeu_si128((__m128i *)&in[k + 4], _mm_unpackhi_epi64(e, o));
    }
  }

  // same as previous stage, except data is already in continuous vectors
  for (size_t m = 8; m <= n; m <<= 1) {
    twd++;
    for (size_t k = 0; k < n; k += m) {
      for (size_t j = 0; j < m / 2; j += 4) {
        __m128i ve = _mm_loadu_si128((__m128i *)&in[k + j]);
        __m128i vo = _mm_loadu_si128((__m128i *)&in[k + j + m / 2]);
        __m128i vw = _mm_loadu_si128((__m128i *)((*twd) + j));

        ve = _mm_srai_epi16(ve, 1);

        __m128i vt = vcmul4(vo, vw);
        vo = _mm_subs_epi16(ve, vt);
        ve = _mm_adds_epi16(ve, vt);

        _mm_storeu_si128((__m128i *)(&in[k + j]), ve);
        _mm_storeu_si128((__m128i *)(&in[k + j + m / 2]), vo);
      }
    }
  }
}

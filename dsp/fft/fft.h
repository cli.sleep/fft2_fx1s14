#pragma once
#include <stddef.h>
#include <stdint.h>

/**
 * @brief Number fractional bits in the fixed point number
 *
 */
#define FRAC_BITS (14)

/**
 * @brief Complex sample structure
 *
 */
typedef struct __attribute__((packed)) {
  int16_t r; /**< real part */
  int16_t i; /**< imaginary part */
} fft_cplx_t;

/**
 * @brief Calculate Fourier transform
 *
 * @param in Input array of complex samples
 * @param len Number of samples in input array
 */
void fft_fx1s14(fft_cplx_t *in, size_t len);

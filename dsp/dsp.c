#include <stdint.h>
#include <stdio.h>

#include "dma/dma.h"
#include "dsp.h"
#include "fft/fft.h"

// Buffer of complex samples for FFT
static fft_cplx_t fft_buf[DSP_MAX_FFT_LEN];

void receive_message(dsp_msg_t const *const m) {
  assert(NULL != m);

  switch (m->type) {
  case DSP_MSG_TYPE_FFT_SET_INPUT:
    dma_copy_rev((uint8_t *)fft_buf, m->args.fft_set_input.src, 1,
                 m->args.fft_set_input.src_step, sizeof(fft_cplx_t),
                 m->args.fft_set_input.len);
    break;
  case DSP_MSG_TYPE_FFT_RUN:
    fft_fx1s14(fft_buf, m->args.fft_run.len);
    break;
  case DSP_MSG_TYPE_FFT_GET_OUTPUT:
    dma_copy(m->args.fft_get_output.dst, (uint8_t *)fft_buf,
             m->args.fft_get_output.dst_step, 1, sizeof(fft_cplx_t),
             m->args.fft_get_output.len);
    break;
  default:
    assert(0);
    break;
  }

  release_message(m);
}

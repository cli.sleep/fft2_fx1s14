#pragma once
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>

#define DSP_MAX_FFT_LEN (512u)

/**
 * @brief DSP message structure
 *
 */
typedef struct {
  enum {
    DSP_MSG_TYPE_FFT_SET_INPUT,
    DSP_MSG_TYPE_FFT_RUN,
    DSP_MSG_TYPE_FFT_GET_OUTPUT,
  } type;
  union {
    struct args_fft_set_input {
      void *src;
      size_t src_step;
      size_t len;
    } fft_set_input;
    struct args_fft_run {
      size_t len;
    } fft_run;
    struct args_fft_get_output {
      void *dst;
      size_t dst_step;
      size_t len;
    } fft_get_output;
  } args;
} dsp_msg_t;

/**
 * @brief Handle messages sent from host
 *
 * @param m pointer to the message of type `dsp_msg_t`
 */
void receive_message(dsp_msg_t const *const m);

/**
 * @brief Inform host that a message is processed and ready
 *
 * @param m pointer to the message of type `dsp_msg_t`
 */
void release_message(dsp_msg_t const *const m);

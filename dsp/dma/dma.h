#pragma once
#include <stddef.h>
#include <stdint.h>

/**
 * @brief Copy data blocks of size `len`, repeat `count` number of times
 *
 * @param dst      Destination memory pointer
 * @param src      Source memory pointer
 * @param dst_step Interval between consequent blocks of destination data in
 *                 units of `len`
 * @param src_step Interval between consequent blocks of source data in units of
 *                 `len`
 * @param len      Size of a single data block
 * @param count    Total number of blocks to copy
 */
void dma_copy(uint8_t *dst, uint8_t *src, size_t dst_step, size_t src_step,
              size_t len, size_t count);

/**
 * @brief Bit reverse copy data blocks of size `len`, repeat `count` number of
 * times
 *
 * @param dst      Destination memory pointer
 * @param src      Source memory pointer
 * @param dst_step Interval between consequent blocks of destination data in
 *                 units of `len`
 * @param src_step Interval between consequent blocks of source data in units of
 *                 `len`
 * @param len      Size of a single data block
 * @param count    Total number of blocks to copy
 */
void dma_copy_rev(uint8_t *dst, uint8_t *src, size_t dst_step, size_t src_step,
                  size_t len, size_t count);

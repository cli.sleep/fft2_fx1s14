#include <assert.h>
#include <string.h>

#include "../fft/fft.h"
#include "brev_lut.h"
#include "dma.h"

#define DMA_MAX_XFER_BYTES (2048 / 8)
#define DMA_MAX_CHANNELS (16)

static void dma_xfer(void *dst, const void *src, size_t n) {
  assert(n <= DMA_MAX_XFER_BYTES);
  memcpy(dst, src, n);
}

void dma_copy(uint8_t *dst, uint8_t *src, size_t dst_step, size_t src_step,
              size_t len, size_t count) {
  while (count--) {
    dma_xfer(dst, src, len);
    src += src_step * len;
    dst += dst_step * len;
  }
}

void dma_copy_rev(uint8_t *dst, uint8_t *src, size_t dst_step, size_t src_step,
                  size_t len, size_t count) {
  // Bit reverse lookup table stride
  size_t brev_stride = 0;

  // For maximum size, stride is 0, othervise:
  // brev_stride = log2(brev_lut_max) - log2(count);
  for (size_t i = count; i < brev_lut_max; i <<= 1)
    brev_stride++;

  for (size_t i = 0; i < count; i++) {
    size_t j = brev_lut[i << brev_stride];
    dma_xfer(dst + dst_step * len * j, src, len);
    src += src_step * len;
  }
}

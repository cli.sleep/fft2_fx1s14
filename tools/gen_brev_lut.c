// Bit reverse lookup table generator

#include <stdint.h>
#include <stdio.h>

size_t brevi(size_t i, size_t n) {
  size_t r = 0;
  while (n--) {
    r <<= 1;
    r |= i & 0x1;
    i >>= 1;
  }
  return r;
}

int main() {
  size_t n = 512;
  size_t logn = 9;
  printf("const uint16_t brev_lut_max = %zu;\n", n);
  printf("const uint16_t brev_lut_log2 = %zu;\n", logn);
  printf("const uint16_t brev_lut[%zu] = {", n);
  for (size_t i = 0; i < n; i++) {
    if (i % 16 == 0)
      printf("\n");
    printf("%3zu, ", brevi(i, logn));
  }
  printf("\n};\n");
}

// Twiddle factor lookup table generator

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

int main(void) {
  const size_t max_len = 512;
  const size_t frac_bits = 14;
  for (size_t n = 4; n <= 512; n *= 2) {
    printf("const int16_t twd_lut_%zu[][2] = {", n);
    for (size_t i = 0; i < n / 2; i++) {
      double w_r = cos(-2.0 * M_PI * i / n);
      double w_i = sin(-2.0 * M_PI * i / n);
      if (i % 4 == 0)
        printf("\n  ");
      printf("{%6d, %6d}, ", (int16_t)(w_r * pow(2, frac_bits)),
             (int16_t)(w_i * pow(2, frac_bits)));
    }
    printf("\n};\n");
  }
  printf("const size_t twd_lut_max = %zu;\n", max_len);
  printf("const size_t twd_lut_frac_bits = %zu;\n", frac_bits);
  printf("const int16_t (*twd_lut[])[2] = {\n");
  for (size_t n = 4; n <= 512; n *= 2) {
    printf("  twd_lut_%zu,\n", n);
  }
  printf("};\n");
}

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../dsp/dma/dma.h"
#include "../dsp/fft/fft.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

// Fixed to floating point conversion
static void fx1s14_to_f32(complex float *f, fft_cplx_t *i, size_t n) {
  while (n--) {
    *f = (i->r) * powf(2, -FRAC_BITS) + (i->i) * I * powf(2, -FRAC_BITS);
    f++;
    i++;
  }
}

// Float to fixed point conversion
static void f32_to_fx1s14(complex float *f, fft_cplx_t *i, size_t n) {
  while (n--) {
    i->r = crealf(*f) * powf(2, FRAC_BITS);
    i->i = cimagf(*f) * powf(2, FRAC_BITS);
    f++;
    i++;
  }
}

static void fft_f32(complex float *out, complex float *in, size_t s, size_t n) {
  if (n == 1) {
    out[0] = in[0];
  } else {
    fft_f32(out, in, s * 2, n / 2);
    fft_f32(out + n / 2, in + s, s << 1, n >> 1);

    for (size_t i = 0; i < n / 2; i++) {
      complex float w = cexpf(-2.0f * (float)M_PI * I * i / n);
      complex float e = out[i];
      complex float o = out[i + n / 2];
      e /= 2;
      o /= 2;
      complex float t = w * o;
      out[i] = e + t;
      out[i + n / 2] = e - t;
    }
  }
}

// Verify that the output array is the Fourier transform of the input
static bool verify(fft_cplx_t *in, fft_cplx_t *out, size_t n, int max_error) {
  bool same = true;
  complex float fin[n];
  complex float fout[n];

  fx1s14_to_f32(fin, in, n);
  fft_f32(fout, fin, 1, n);

  fft_cplx_t iout[n];
  f32_to_fx1s14(fout, iout, n);

  for (size_t i = 0; i < n; i++) {
    int dr = out[i].r - iout[i].r;
    int di = out[i].i - iout[i].i;
    if (abs(dr) > max_error || abs(di) > max_error) {
      same = false;
      printf("diff %d %d at index %zu for input size %zu\n", dr, di, i, n);
    }
  }

  return same;
}

// Fills the input array with random complex samples
static void gen_random_wave(fft_cplx_t *in, size_t n) {
  for (size_t i = 0; i < n; i++) {
    in[i].r = (int16_t)rand();
    in[i].i = (int16_t)rand();
  }
}

int main() {
  srand(time(NULL));

  for (size_t n = 8; n <= 512; n *= 2) {
    bool pass = false;
    for (size_t i = 0; i < 256; i++) {
      fft_cplx_t in[n];
      gen_random_wave(in, n);

      fft_cplx_t out[n];
      // copy the input wave in bit reverse order
      dma_copy_rev((uint8_t *)out, (uint8_t *)in, 1, 1, sizeof(fft_cplx_t), n);
      // run fixed point FFT
      fft_fx1s14(out, n);

      // allow error of 5 fractional units
      int max_error = 5;
      pass = verify(in, out, n, max_error);
    }
    printf("Test of input size %zu %s\n", n, pass ? "PASSED" : "FAILED");
  }
}

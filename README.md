# Fixed point 2D FFT
DSP accelerated Fast Fourier transform for 16 bit fixed point data of size
M by N, where N,M are in the range of [8,512].

# Sources
* `dsp/` folder contains implementations of DSP functions
* `host.c` demonstrates example usage of DSP functions to calculate 2D FFT
* `test/test.c` simple test for FFT and bit reverse copy algorithms
* `tools/` folder contains programs used to generate lookup tables

# Prerequisites
* Intel SSE2 and SSSE3 support for FFT
* C math library for tests and lookup table generators

# Implementation
Due to data memory limitations of the DSP (8kB), it is impossible to transfer
full sized input matrix to it. For this reason, DSP only implements a
1-dimensional FFT. To get 2D FFT, host calculates FFT first on rows then on
columns of the input matrix.

## Simplifications
* Non power of 2 input sizes are not allowed.
* Asynchronous nature of host to DSP communication is not modeled.

## FFT algorithm
DSP implements iterative radix-2 Cooley–Tukey FFT algorithm, it expects that
the input data is in bit-reverse order and calculates the transform in-place
without using additional buffers. Input size is restricted to powers of 2 in
the range of [8,512]. Twiddle factors for all possible inputs are
pre-calculated and stored in DSP memory. Twiddle factors for the biggest 
input size already contain the factors of smaller sizes, but storing them
separately improves speed at the cost of additional memory.

## Input scaling
To avoid saturation, input data is scaled down by 2 at each FFT stage.

## SIMD
SIMD instructions from Intel SSE2 and SSSE3 extensions are used to model the
128 bit instructions available on the DSP.

## DMA
DMA is used to implement two types of copy operations, one in normal order
and on in reverse bit order. Bit reversal permutation for biggest allowed
input size is pre calculated and stored in a lookup table.

## Host-DSP interface
Host sends commands to the DSP through a messaging interface. Asynchronous
nature of this interface is not modeled for simplicity.

Available DSP commands are:
* FFT_SET_INPUT: fills FFT buffer with data provided by host in bit reverse
order
* FFT_RUN: calculates FFT on data that is currently in the buffer
* FFT_GET_OUTPUT: transfers FFT buffer content to a destination provided by
host

The FFT_SET_INPUT and FFT_GET_OUTPUT commands support custom source/destination
steps, this allows to copy interleaved input data to a continuous FFT buffer.
This is useful for FFT calculation on columns.

## Testing
Transform results are compared to results of a 'textbook' FFT implementation
for floating point data. This is a naive approach, instead, mathematical
properties of Fourier transform should be used for verification.

# Further improvements
List of unfinished features in the order of importance.

## Better testing
Test correctness of results based on mathematical properties of Fourier
transform.

## DMA double buffering
The DSP has enough memory to allocate a second FFT buffer. This way, while the
core is busy calculating FFT on buffer A, the DMA gets results and transfer new
data to buffer B in the background.

## Split format for complex data
Performance of complex arithmetic can be improved significantly if the input
data is kept (or transformed on the fly) to a split complex format. Instead
of interleaving the real and imaginary parts of samples, they can be kept in
separate vectors, this will eliminate the need for shuffle instructions.

## Better algorithms
Radix-2 Cooley–Tukey FFT algorithm is the simplest in the family of FFT
algorithms, explore better alternatives.

## Optimize for real input data
Transforms of size N with no complex components, can be reduced to transforms
of size N-1 with complex components.

.PHONY: default format clean

CFLAGS = -mssse3 -std=c99 -pedantic -Wall -Wextra

default: host.out test.out
	./test.out
	./host.out

test.out: test/test.c dsp/fft/fft.c dsp/dma/dma.c
	cc ${CFLAGS} $^ -lm -o $@

host.out: host.c dsp/dsp.c dsp/fft/fft.c dsp/dma/dma.c
	cc ${CFLAGS} $^ -o $@
	size $@

format:
	fd -e .c -e .h | xargs clang-format -i

clean:
	rm -f *.out

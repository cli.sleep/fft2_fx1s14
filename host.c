#include <time.h>

#include "dsp/dsp.h"
#include "dsp/fft/fft.h"

// Number of cores available to the host
enum {
  DSP_CORE0 = 0,
  DSP_CORES_MAX,
};

static void init_core(uint16_t const core_id) {
  assert(core_id < DSP_CORES_MAX);
}

// Sends a message to the core. Currently calls the `receive_message` directly.
static void send_message(uint16_t const core_id, dsp_msg_t const *const m) {
  assert(core_id < DSP_CORES_MAX);
  assert(NULL != m);
  receive_message(m);
}

// Callback from DSP, to inform that a message is processed and ready.
void release_message(dsp_msg_t const *const m) { assert(NULL != m); }

// Helper function for setting the input of FFT
static void fft_set_input(uint16_t core_id, void *src, size_t src_step,
                          size_t len) {
  dsp_msg_t m;
  m.type = DSP_MSG_TYPE_FFT_SET_INPUT;
  m.args.fft_set_input.src = src;
  m.args.fft_set_input.src_step = src_step;
  m.args.fft_set_input.len = len;
  send_message(core_id, &m);
}

// Helper function for getting the output of FFT
static void fft_get_output(uint16_t core_id, void *dst, size_t dst_step,
                           size_t len) {
  dsp_msg_t m;
  m.type = DSP_MSG_TYPE_FFT_GET_OUTPUT;
  m.args.fft_get_output.dst = dst;
  m.args.fft_get_output.dst_step = dst_step;
  m.args.fft_get_output.len = len;
  send_message(core_id, &m);
}

// Helper function for calculation of FFT on current buffer
static void fft_run(uint16_t core_id, size_t len) {
  dsp_msg_t m;
  m.type = DSP_MSG_TYPE_FFT_RUN;
  m.args.fft_run.len = len;
  send_message(core_id, &m);
}

static void gen_random_input(fft_cplx_t *in, size_t row, size_t col) {
  while (row--) {
    for (size_t i = 0; i < col; i++) {
      in->r = (int16_t)rand();
      in->i = 0;
      in++;
    }
  }
}

int main() {
  srand(time(NULL));

  init_core(DSP_CORE0);

  for (size_t row = 8; row <= DSP_MAX_FFT_LEN; row *= 2) {
    for (size_t col = 8; col <= DSP_MAX_FFT_LEN; col *= 2) {
      fft_cplx_t in[row][col];
      gen_random_input(&in[0][0], row, col);

      // Transform rows
      for (size_t i = 0; i < row; i++) {
        fft_set_input(DSP_CORE0, in[i], 1, col);
        fft_run(DSP_CORE0, col);
        fft_get_output(DSP_CORE0, in[i], 1, col);
      }

      // Transform columns
      for (size_t i = 0; i < col; i++) {
        fft_set_input(DSP_CORE0, &in[0][i], col, row);
        fft_run(DSP_CORE0, row);
        fft_get_output(DSP_CORE0, &in[0][i], col, row);
      }
    }
  }

  return 0;
}
